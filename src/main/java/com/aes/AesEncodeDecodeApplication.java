package com.aes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AesEncodeDecodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AesEncodeDecodeApplication.class, args);
	}

}
